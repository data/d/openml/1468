# OpenML dataset: cnae-9

https://www.openml.org/d/1468

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Patrick Marques Ciarelli, Elias Oliviera   
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/CNAE-9) - 2010  
**Please cite**:   

### Description

This is a data set containing 1080 documents of free text business descriptions of Brazilian companies categorized into a subset of 9 categories.

### Source
```
Patrick Marques Ciarelli, pciarelli '@' lcad.inf.ufes.br, Department of Electrical Engineering, Federal University of Espirito Santo 
Elias Oliveira, elias '@' lcad.inf.ufes.br, Department of Information Science, Federal University of Espirito Santo
```

### Data Set Information

This is a data set containing 1080 documents of free text business descriptions of Brazilian companies categorized into a 
subset of 9 categories cataloged in a table called National Classification of Economic Activities (Classificação Nacional de 
Atividade Econômicas - CNAE). The original texts were preprocessed to obtain the current data set: initially, it was kept only letters and then it was removed prepositions of the texts. Next, the words were transformed to their canonical form. Finally, 
each document was represented as a vector, where the weight of each word is its frequency in the document. 
This data set is highly sparse (99.22% of the matrix is filled with zeros).

### Attribute Information

In the dataset there are 857 attributes, 1 attributes with the class of instance and 856 with word frequency:
 ```
1. category: range 1 - 9 (integer)   
2. 857. word frequency: (integer)
```

### Relevant Papers

Patrick Marques Ciarelli, Elias Oliveira, 'Agglomeration and Elimination of Terms for Dimensionality Reduction', 
Ninth International Conference on Intelligent Systems Design and Applications, pp.547-552, 2009 

Patrick Marques Ciarelli, Elias Oliveira, Evandro O. T. Salles, 'An Evolving System Based on Probabilistic Neural Network', 
Brazilian Symposium on Artificial Neural Network, 2010

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1468) of an [OpenML dataset](https://www.openml.org/d/1468). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1468/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1468/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1468/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

